package week0;

import java.util.Scanner;

public class ConditionalOperations {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the value of number1:");
		Scanner scan1=new Scanner(System.in);
		int number1=scan1.nextInt();
		System.out.println("Enter the value of number2:");
		Scanner scan2=new Scanner(System.in);
		int number2=scan2.nextInt();
		System.out.println("Is number1 is equal to number2: " + (number1==number2));
		System.out.println("Is number1 is notequal to number2:" + (number1!=number2));
		System.out.println("Is number1 is lesser than number2:"+ (number1<number2));
		System.out.println("Is number1 is lesser than or equal to number2:"+ (number1<=number2));
		System.out.println("Is number1 is greater than number2:"+ (number1>number2));
		System.out.println("Is number1 is greater than or equal to number2:"+ (number1>=number2));
	}

}
