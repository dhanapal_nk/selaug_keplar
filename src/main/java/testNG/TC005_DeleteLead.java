package testNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import wdmethods.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods{
	
	@BeforeTest(groups= {"sanity"},dependsOnGroups= {"smoke"})
	public void setData1() {
		testCaseName = "TC005_DeleteLead";
		testCaseDesc = "Delete a lead";
		category = "smoke";
		author = "Dhanapal";
	}
	@Test(groups= {"sanity"}, dataProvider="positive")
	public void deleteLead(String fName) throws InterruptedException{
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		click(locateElement("xpath","//span[text()='Name and ID']"));
		type(locateElement("name","firstName"), fName);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadId = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		click(locateElement("linkText","Delete"));
		click(locateElement("linkText","Find Leads"));
		type(locateElement("name", "id"), leadId);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		verifyExactText(locateElement("xpath","//div[@class='x-paging-info']"), "No records to display");
		closeBrowser();	
	}
	
	@DataProvider(name="positive")
	public Object[][] fetchData(){
		Object[][] data = new Object[1][1];

		data[0][0]="dhanapal";

		return data;
	}

	@DataProvider(name="negative")
	public void fetchNegData(){

	}

}

