package testNG;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdmethods.ProjectMethods;

public class TC006_DulpicateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"regression"})
	public void setData1() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc = "Create a new lead";
		category = "smoke";
		author = "arul";
	}
	@Test(groups= {"regression"},dataProvider="positive")
	public void duplicate(String fName) throws InterruptedException {
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		click(locateElement("xpath","//span[text()='Name and ID']"));
		type(locateElement("name","firstName"), fName);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		String leadName = getText(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a"));
		click(locateElement("linkText","Duplicate Lead"));
		verifyTitle("Duplicate Lead");
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id","viewLead_firstName_sp"), leadName);
		closeBrowser();	
	}
	
	@DataProvider(name="positive")
	public Object[][] fetchData(){
		Object[][] data = new Object[1][1];
		
		data[0][0]="dhanapal";
		
		return data;
	}
	
	@DataProvider(name="negative")
	public void fetchNegData(){
	
	}

}
