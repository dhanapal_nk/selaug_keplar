package testNG;

import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;

import utils.ReadExcel;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdmethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testCaseName="TC002_CreateLead";
		testCaseDesc="Create a New Lead";
		category="smoke";
		author="Dhanapal";
		fileName="CL";
	}

	@Test(groups= {"smoke"},dataProvider="fetchData")
	public void CLead(String cName, String fName, String lName)
	{

		WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		WebElement eleCL = locateElement("linkText", "Create Lead");
		click(eleCL);
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, cName);
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, fName);
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, lName);
/*		WebElement eleDropData = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDropData,"Employee");
		WebElement eleDropMark = locateElement("id","createLeadForm_marketingCampaignId");
		selectDropDownUsingText(eleDropMark,"Pay Per Click Advertising");
		WebElement eleFirstNameLocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(eleFirstNameLocal, "Dhanapal");
		WebElement eleLastNameLocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLastNameLocal, "N K");
		WebElement eleSalutation = locateElement("id", "createLeadForm_personalTitle");
		type(eleSalutation, "Mr");
		WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleTitle, "Software Engineer");
		WebElement eleDepart = locateElement("id", "createLeadForm_departmentName");
		type(eleDepart, "Information Technology");
		WebElement eleRevenue = locateElement("id", "createLeadForm_annualRevenue");
		type(eleRevenue, "400000");
		WebElement eleDropCurrency = locateElement("id","createLeadForm_currencyUomId");
		selectDropDownUsingText(eleDropCurrency,"INR - Indian Rupee");
		WebElement eleIndustry = locateElement("id","createLeadForm_industryEnumId");
		selectDropDownUsingIndex(eleIndustry, 2);
		WebElement eleNoofEmp = locateElement("id", "createLeadForm_numberEmployees");
		type(eleNoofEmp, "700");
		WebElement eleDropOwnership = locateElement("id","createLeadForm_ownershipEnumId");
		selectDropDownUsingText(eleDropOwnership,"Sole Proprietorship");*/
		WebElement eleCreate = locateElement("class", "smallSubmit");
		click(eleCreate);
	}

}
