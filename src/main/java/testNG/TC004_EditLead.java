package testNG;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import wdmethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods{
	@BeforeTest(groups= {"sanity"},dependsOnGroups= {"smoke"})
	public void setData1()
	{
		testCaseName="TC004_EditLead";
		testCaseDesc="Edit the Lead";
		category="smoke";
		author="Dhanapal";
		fileName="EditLead";
	}
	@Test(groups= {"sanity"},dataProvider="fetchData")
	public void editLead(String fName, String cName) throws InterruptedException {
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		verifyTitle("View Leads");
		click(locateElement("linkText","Edit"));
		locateElement("id", "updateLeadForm_companyName").clear();
		type(locateElement("id", "updateLeadForm_companyName"),cName);
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id", "viewLead_companyName_sp"), "Synfusion Software");
		closeBrowser();		
	}
	
}
