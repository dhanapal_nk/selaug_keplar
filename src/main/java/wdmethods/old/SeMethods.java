package wdmethods.old;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;


public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver  = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		} else if(browser.equalsIgnoreCase("ie")) {
			System.setProperty("webdriver.ie.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		} else if(browser.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver", "./drivers/microsoftwebdriver.exe");
			driver  = new FirefoxDriver();
		} else if(browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		}
		else if(browser.equalsIgnoreCase("opera")) {
			System.setProperty("webdriver.opera.driver", "./drivers/operadriver.exe");
			driver  = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched successfully");
		takeSnap();

	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case "name": return driver.findElementByName(locValue);
		case "tag": return driver.findElementByTagName(locValue);
		case "css": return driver.findElementByCssSelector(locValue);
		case "partialtext" : return driver.findElementByPartialLinkText(locValue);
		}
		takeSnap();
		return null;
	}

	
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}
	
	public void clickWithoutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
	}

	
	public String getText(WebElement ele) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		Select dd=new Select(ele);
		dd.selectByVisibleText(value);
		takeSnap();
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		Select dd=new Select(ele);
		dd.selectByIndex(index);
		takeSnap();
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		// TODO Auto-generated method stub
		String title = driver.getTitle();
		boolean bReturn=false;
		if(title.equals(expectedTitle))
		{
			bReturn=true;
		}
		return bReturn;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		if(text.equals(expectedText))
		{
			System.out.println("Both text are same");
		}
		takeSnap();
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		String text = ele.getText();
		if(text.contains(expectedText))
		{
			System.out.println("Given text is present");
		}
		takeSnap();
	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attValue = ele.getAttribute(attribute);
		if(attValue.equals(value))
		{
			System.out.println("Attribute value is matching");
		}
		takeSnap();
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		String attValue = ele.getAttribute(attribute);
		if(attValue.contains(value))
		{
			System.out.println("Attribute contains the given value");
		}
		takeSnap();
	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		boolean selected = ele.isSelected();
		if(selected)
		{
			System.out.println("True");
		} else
		{
			System.out.println("False");
		}
		takeSnap();
	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		boolean displayed = ele.isDisplayed();
		if(displayed)
		{
			System.out.println("True");
		} else
		{
			System.out.println("False");
		}
		takeSnap();
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> allWindows= new ArrayList<String>();
		allWindows.addAll(windowHandles);
		driver.switchTo().window(allWindows.get(index));
		takeSnap();
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		WebDriver frame = driver.switchTo().frame(ele);
		System.out.println("Switched to selected frame");
		takeSnap();
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().accept();
		System.out.println("Alert is Accepted");
		takeSnap();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();
		System.out.println("Alert is closed");
		takeSnap();
		
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		String text = driver.switchTo().alert().getText();
		return text;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".png");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
		System.out.println("Window is closed successfully");
		takeSnap();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.close();
		System.out.println("Window is closed successfully");
		takeSnap();
	}

}
