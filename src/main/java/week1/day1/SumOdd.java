package week1.day1;

import java.util.Scanner;

public class SumOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum=0;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the no of elements in array: ");
		int n=scan.nextInt();
		System.out.println("Enter all the elements:");
		int a[]=new int[n];
		for (int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}
		//Oddnumber Check
		for (int i=0;i<n;i++)
		{
			if (a[i]%2!=0)
			{
				sum=sum+a[i];
			}
		}
		System.out.println(sum);
		
	}

}
