package dailywork;

import java.util.Scanner;

public class PalindromeString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String rev="";
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the input string");
		String str=scan.next();
		int len=str.length();
		//Reverse
		for (int i=len-1;i>=0;i--)
		{
			rev=rev+str.charAt(i);
		}
		
		if(rev.equals(str))
		{
			System.out.println("Given String is Palindrome");
		}
		else
		{
			System.out.println("Given String is not Palindrome");
		}
		
	}

}
