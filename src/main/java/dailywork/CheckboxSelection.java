package dailywork;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CheckboxSelection {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/checkbox.html");
		WebElement checkBox = driver.findElementByXPath("//*[@id=\"contentblock\"]/section/div[1]/input[1]");
		//checkBox.click();
		if(checkBox.isSelected()==true)
		{
			System.out.println("Checkbox is checked");
		}
		else
		{
			System.out.println("Checkbox is unchecked");
		}
	}
}
