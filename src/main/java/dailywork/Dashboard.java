package dailywork;

import org.openqa.selenium.chrome.ChromeDriver;

public class Dashboard {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver= new ChromeDriver();
		driver.get("http://localhost:63790");
		driver.manage().window().maximize();
		driver.findElementById("ump-proceed").click();
	}
}
