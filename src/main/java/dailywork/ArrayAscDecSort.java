package dailywork;

import java.util.Scanner;

public class ArrayAscDecSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int temp;
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter the no of elements in array: ");
		int n=scan.nextInt();
		System.out.println("Enter all the elements:");
		int a[]=new int[n];
		for (int i=0;i<n;i++)
		{
			a[i]=scan.nextInt();
		}
		//Sorting
		for (int i=0;i<n;i++)
		{
			for(int j=i+1;j<n;j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		System.out.println("Ascendig order of Array elements:");
		for(int i=0;i<n;i++)
		{
			System.out.println(a[i]);
		}
		for (int i=0;i<n;i++)
		{
			for(int j=i+1;j<n;j++)
			{
				if(a[i]<a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
		}
		System.out.println("Descending order of Array elements:");
		for(int i=0;i<n;i++)
		{
			System.out.println(a[i]);
		}
	}

}
