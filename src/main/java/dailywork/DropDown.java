package dailywork;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Dropdown.html");
		WebElement src = driver.findElementById("dropdown1");
		Select dropDown=new Select(src);
		List<WebElement> eachOptions = dropDown.getOptions();
		int length=eachOptions.size();
		dropDown.selectByIndex(length-2);

	}
}
