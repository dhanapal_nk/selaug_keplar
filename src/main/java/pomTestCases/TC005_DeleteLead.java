package pomTestCases;

import org.testng.annotations.Test;
import pages.LoginPage;
import org.testng.annotations.BeforeTest;
import wdmethods.ProjectMethods;

public class TC005_DeleteLead extends ProjectMethods{

	@BeforeTest()
	public void setData() {
		testCaseName="Delete Lead";
		testCaseDesc="To Delete Leads in Opentaps";
		category="smoke";
		author="Dhanapal";
		fileName="TC005";
	}

	@Test(dataProvider="fetchData")
	public void login(String username, String password, String vUsername, String firstName ) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.verifyLoggedInName(vUsername)
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeFirstname(firstName)
		.clickFindLeads()
		.clickFirstLead()
		.clickDeleteButton();	
	}
}

