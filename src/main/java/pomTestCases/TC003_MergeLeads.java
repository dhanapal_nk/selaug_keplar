package pomTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdmethods.ProjectMethods;

public class TC003_MergeLeads extends ProjectMethods{

	@BeforeTest()
	public void setData() {
		testCaseName="Merge Lead";
		testCaseDesc="To Merge Leads in Opentaps";
		category="smoke";
		author="Dhanapal";
		fileName="TC003";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String vUsername, String fName, String sName ) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.verifyLoggedInName(vUsername)
		.clickCRMSFA()
		.clickLeads()
		.clickMergeLead()
		.SelectFirstLead()
		.typeFirstname(fName)
		.clickFindLeads()
		.clickFirstLead()
		.SelectSecondLead()
		.typeSecondname(sName)
		.clickFindLeads()
		.clickSecondLead();

	}
}
