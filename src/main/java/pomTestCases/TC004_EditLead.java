package pomTestCases;

import org.testng.annotations.Test;
import pages.LoginPage;
import org.testng.annotations.BeforeTest;
import wdmethods.ProjectMethods;

public class TC004_EditLead extends ProjectMethods{

	@BeforeTest()
	public void setData() {
		testCaseName="Edit Lead";
		testCaseDesc="To Edit Leads in Opentaps";
		category="smoke";
		author="Dhanapal";
		fileName="TC004";
	}

	@Test(dataProvider="fetchData")
	public void login(String username, String password, String vUsername, String firstName, String proTitle ) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.verifyLoggedInName(vUsername)
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeFirstname(firstName)
		.clickFindLeads()
		.clickFirstLead()
		.clickEditButton()
		.updateProfileTitle(proTitle)
		.clickUpdateButton();
	}

}
