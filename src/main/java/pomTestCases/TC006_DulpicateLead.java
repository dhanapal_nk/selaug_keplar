package pomTestCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdmethods.ProjectMethods;

public class TC006_DulpicateLead extends ProjectMethods{
	
	@BeforeTest()
	public void setData() {
		testCaseName="Duplicate Lead";
		testCaseDesc="To Create Duplicate Leads in Opentaps";
		category="smoke";
		author="Dhanapal";
		fileName="TC006";
	}

	@Test(dataProvider="fetchData")
	public void login(String username, String password, String vUsername, String firstName) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.verifyLoggedInName(vUsername)
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.typeFirstname(firstName)
		.clickFindLeads()
		.clickFirstLead()
		.clickDuplicateLead()
		.clickCreatLeadButton();
	}

}
