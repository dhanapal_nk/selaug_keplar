package pomTestCases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdmethods.ProjectMethods;


public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest()
	public void setData() {
		testCaseName="Create Lead";
		testCaseDesc="To crete Lead in Opentaps";
		category="smoke";
		author="Dhanapal";
		fileName="TC002";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String vUsername, String cName, String fName, String lName, String vFName) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.verifyLoggedInName(vUsername)
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCLButton()
		.verifyFirstName(vFName);
	}
	
}







