package pomTestCases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdmethods.ProjectMethods;


public class TC001_LoginAndLogOut extends ProjectMethods{
	
	@BeforeTest()
	public void setData() {
		testCaseName="Login and LogOut";
		testCaseDesc="To Login and Logout in Opentaps";
		category="smoke";
		author="Dhanapal";
		fileName="TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String vUsername) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.verifyLoggedInName(vUsername)
		.clickLogout();
		
	}
	
}







