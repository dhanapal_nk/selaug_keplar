package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdmethods.ProjectMethods;

public class MyLeads extends ProjectMethods {

	public MyLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Create Lead")
	WebElement eleCL;
	@And("Click on CreateLead")
	public CreateLead clickCreateLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleCL);
		return new CreateLead();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Merge Leads")
	WebElement eleML;
	public MergeLeads clickMergeLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleML);
		return new MergeLeads();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Find Leads")
	WebElement eleFL;
	public FindLeads clickFindLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleFL);
		return new FindLeads();
	}
}
