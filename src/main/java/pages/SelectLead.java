package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class SelectLead extends ProjectMethods{

	public SelectLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//input[@name=\"firstName\"])[3]")
	WebElement eletypeFName;
	public SelectLead typeFirstname(String data) {
		type(eletypeFName, data);
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")
	WebElement eleclickFLButton;
	public SelectLead clickFindLeads() {
		click(eleclickFLButton);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a")
	WebElement eleFirstLead;
	public MergeLeads clickFirstLead() {
		click(eleFirstLead);
		return new MergeLeads();
	}
	
	@FindBy(how=How.XPATH, using="(//input[@name=\"firstName\"])[3]")
	WebElement eletypeSName;
	public SelectLead typeSecondname(String data) {
		type(eletypeSName, data);
		return this;
	}
	
	/*@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")
	WebElement eleclickSLButton;
	public SelectLead clickFindLeads() {
		click(eleclickSLButton);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}*/
	
	@FindBy(how=How.XPATH, using="//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a")
	WebElement eleSecondLead;
	public MergeLeads clickSecondLead() {
		click(eleSecondLead);
		return new MergeLeads();
	}
}
