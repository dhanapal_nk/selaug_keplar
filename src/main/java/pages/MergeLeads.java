package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import wdmethods.ProjectMethods;

public class MergeLeads extends ProjectMethods {

	public MergeLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[1]")
	WebElement eleFLead;
	public SelectLead SelectFirstLead() {
		click(eleFLead);
		switchToWindow(1);
		return new SelectLead();
	}
	
	@FindBy(how=How.XPATH, using="(//img[@src='/images/fieldlookup.gif'])[2]")
	WebElement eleSLead;
	public SelectLead SelectSecondLead() {
		click(eleSLead);
		switchToWindow(1);
		return new SelectLead();
	}
		
}
