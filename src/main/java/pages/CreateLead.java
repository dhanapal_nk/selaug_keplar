package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdmethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	
	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCompanyName;
	@And("Enter the Company Name as (.*)")
	public CreateLead typeCompanyName(String data) {
	type(eleCompanyName, data);
	return this;
	}
	
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFirstName;
	@And("Enter the First Name as (.*)")
	public CreateLead typeFirstName(String data) {
	type(eleFirstName, data);
	return this;
	}
	
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLastName;
	@And("Enter the Last Name as (.*)")
	public CreateLead typeLastName(String data) {
	type(eleLastName, data);
	return this;
	}
	
	@FindBy(how=How.CLASS_NAME, using="smallSubmit")
	WebElement eleClickCL;
	@And("Click on Creat Lead Button")
	public ViewLead clickCLButton() {
	click(eleClickCL);
	return new ViewLead();
	}

}
