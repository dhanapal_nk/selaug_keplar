package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdmethods.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="viewLead_firstName_sp")
	WebElement elevfName;
	@When("New Lead is created with first name as (.*)")
	public ViewLead verifyFirstName(String data){
		//WebElement elevUsername = locateElement("xpath","//h2[contains(text(),'Demo')]");
		verifyExactText(elevfName, data);
		return this;
	}
	
	@FindBy(how=How.LINK_TEXT, using="Edit")
	WebElement eleClickEdit;
	public EditLead clickEditButton(){
		//WebElement elevUsername = locateElement("xpath","//h2[contains(text(),'Demo')]");
		click(eleClickEdit);
		return new EditLead();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Delete")
	WebElement eleClickDelete;
	public MyLeads clickDeleteButton(){
		//WebElement elevUsername = locateElement("xpath","//h2[contains(text(),'Demo')]");
		click(eleClickDelete);
		return new MyLeads();
	}
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead")
	WebElement eleClickDupLead;
	public DuplicateLead clickDuplicateLead(){
		//WebElement elevUsername = locateElement("xpath","//h2[contains(text(),'Demo')]");
		click(eleClickDupLead);
		return new DuplicateLead();
	}
}
