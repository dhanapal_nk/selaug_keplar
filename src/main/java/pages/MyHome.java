package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdmethods.ProjectMethods;

public class MyHome extends ProjectMethods {

	public MyHome() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Leads")
	WebElement eleLeads;
	@And("Click on Leads")
	public MyLeads clickLeads() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLeads);
		return new MyLeads();
	}
	
}


