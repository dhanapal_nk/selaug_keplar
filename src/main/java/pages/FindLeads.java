package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class FindLeads extends ProjectMethods{

	public FindLeads() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="(//input[@name=\"firstName\"])[3]")
	WebElement eletypeFName;
	public FindLeads typeFirstname(String data) {
		type(eletypeFName, data);
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']")
	WebElement eleclickFLButton;
	public FindLeads clickFindLeads() {
		click(eleclickFLButton);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	@FindBy(how=How.XPATH, using="//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a")
	WebElement eleFirstLead;
	public ViewLead clickFirstLead() {
		click(eleFirstLead);
		return new ViewLead();
	}
}
