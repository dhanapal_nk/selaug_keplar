package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdmethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="username")
	WebElement eleUsername;
	@And("Enter the Username as (.*)")
	public LoginPage typeUsername(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		type(eleUsername,data);
		return this;
	}
	public LoginPage verifyUsernameColor(String data) {
		verifyExactAttribute(eleUsername, "id", data);
		return this;
	}
	@FindBy(how=How.ID, using="password")
	WebElement elePassword;
	@And("Enter the Password as (.*)")
	public LoginPage typePassword(String data) {
		//WebElement elePassword = locateElement("id", "password");
		type(elePassword,data);
		return this;
	}
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit")
	WebElement eleLogin;
	@And("Click on the login button")
	public HomePage clickLogin() {
		//WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
}
