package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class EditLead extends ProjectMethods {

	public EditLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using="updateLeadForm_generalProfTitle")
	WebElement eleUpdateProTitle;
	public EditLead updateProfileTitle(String data) {
		type(eleUpdateProTitle, data);
		 return this;
	}
	
	@FindBy(how=How.XPATH, using="//input[@type='submit']")
	WebElement eleUpdateButton;
	public ViewLead clickUpdateButton() {
		click(eleUpdateButton);
		 return new ViewLead();
	}
}
