package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdmethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	public HomePage verifyLoggedInName(String data){
		WebElement elevUsername = locateElement("xpath","//h2[text()[contains(.,'Demo')]]");
		verifyPartialText(elevUsername, data);
		return this;
	}
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit")
	WebElement eleLogout;
	public LoginPage clickLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleLogout);
		return new LoginPage();
	}
	@FindBy(how=How.LINK_TEXT, using="CRM/SFA")
	WebElement eleclick;
	@And("Click on crmsfa")
	public MyHome clickCRMSFA()
	{
		click(eleclick);
		return new MyHome();
	}

}
