package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHTMLReports {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/report.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		//testcase level
		ExtentTest test = extent.createTest("TC0001_CreateLead", "Create a new Lead in leaftaps");
		test.assignCategory("Smoke");
		test.assignAuthor("Dhanapal");
		//testcase step level
		test.pass("Browser launched successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The data DemoSalesManager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		test.fail("The data DemoSalesManager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
		test.pass("The element Login is clicked successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img4.png").build());
		extent.flush();
	}

}
