package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class LearnActions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/draggable/");
		Actions builder= new Actions(driver);
		WebElement frame1 = driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frame1);
		WebElement source = driver.findElementById("draggable");
		builder.dragAndDropBy(source, 100, 100).perform();
		
		
		
		

	}

}
