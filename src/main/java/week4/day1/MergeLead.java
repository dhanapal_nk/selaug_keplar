package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> eachWindows = new ArrayList<>();
		eachWindows.addAll(allWindows);
		driver.switchTo().window(eachWindows.get(1));
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10240");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();	
		driver.switchTo().window(eachWindows.get(0));
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		allWindows = driver.getWindowHandles();
		eachWindows = new ArrayList<>();
		eachWindows.addAll(allWindows);
		driver.switchTo().window(eachWindows.get(1));
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10241");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("(//a[@class='linktext'])[1]").click();
		driver.switchTo().window(eachWindows.get(0));
		driver.findElementByXPath("//a[text()='Merge']").click();
		driver.switchTo().alert().accept();
		driver.findElementByLinkText("Find Leads");
		driver.findElementByXPath("(//input[@type='text'])[1]").sendKeys("10223");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		

		
	}

}
