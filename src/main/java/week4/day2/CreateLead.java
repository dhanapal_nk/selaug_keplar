package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.old.SeMethods;

public class CreateLead extends SeMethods {
	@Test
	public void CLead()
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
		click(eleCRM);
		WebElement eleCL = locateElement("linktext", "Create Lead");
		click(eleCL);
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, "ABC");
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, "Dhanapal");
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, "Kuppuswamy");
		WebElement eleDrop = locateElement("id","createLeadForm_dataSourceId");
		selectDropDownUsingText(eleDrop,"Employee");
		WebElement eleCreate = locateElement("class", "smallSubmit");
		click(eleCreate);
		
	}
	
}
