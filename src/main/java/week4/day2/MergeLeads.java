package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdmethods.old.SeMethods;

public class MergeLeads extends SeMethods{
	@Test
	public void MLead() throws InterruptedException
	{
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linktext", "CRM/SFA");
		click(eleCRM);
		WebElement eleLeads = locateElement("xpath","//a[text()='Leads']");
		click(eleLeads);
		WebElement eleMLeads = locateElement("linktext","Merge Leads");
		click(eleMLeads);
		WebElement eleFLead = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFLead);
		switchToWindow(1);
		WebElement eleFLeadID = locateElement("xpath", "//input[@name='id']");
		type(eleFLeadID, "11555");
		WebElement eleFLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFLeads);
		Thread.sleep(3000);
		WebElement eleFRecord = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithoutSnap(eleFRecord);
		switchToWindow(0);
		WebElement eleSLead = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleSLead);
		switchToWindow(1);
		WebElement eleSLeadID = locateElement("xpath", "//input[@name='id']");
		type(eleSLeadID, "10345");
		WebElement eleSLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleSLeads);
		Thread.sleep(3000);
		WebElement eleSRecord = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithoutSnap(eleSRecord);
		switchToWindow(0);
		WebElement eleMerge = locateElement("linktext", "Merge");
		click(eleMerge);
	}
}
