package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver cd=new ChromeDriver();
		//Get URL
		cd.get("http://leaftaps.com/opentaps/");
		//Maximize
		cd.manage().window().maximize();
		//set timeout
		cd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//User name
		try {
			cd.findElementById("username1").sendKeys("DemoSalesManager");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		cd.findElementById("password").sendKeys("crmsfa");
		cd.findElementByClassName("decorativeSubmit").click();
		cd.findElementByLinkText("CRM/SFA").click();
		cd.findElementByLinkText("Create Lead").click();
		cd.findElementById("createLeadForm_companyName").sendKeys("Syncfusion Software");
		cd.findElementById("createLeadForm_firstName").sendKeys("Dhanapal");
		cd.findElementById("createLeadForm_lastName").sendKeys("Kuppuswamy");
		WebElement src = cd.findElementById("createLeadForm_dataSourceId");
		Select dropDown= new Select(src);
		//dropDown.selectByVisibleText("Direct Mail");
		dropDown.selectByValue("LEAD_DIRECTMAIL");
		WebElement mc = cd.findElementById("createLeadForm_marketingCampaignId");
		Select mcDropDown=new Select(mc);
		List<WebElement> options = mcDropDown.getOptions();
		int len=options.size();
		mcDropDown.selectByIndex(len-2);
		cd.findElementByClassName("smallSubmit").click();
		
	}

}
