package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class Table {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/table.html");
		List<WebElement> chkBox = driver.findElementsByXPath("//input[@type='checkbox']");
		int len=chkBox.size();
		System.out.println(len);
		chkBox.get(len-1).click();
	}

}
