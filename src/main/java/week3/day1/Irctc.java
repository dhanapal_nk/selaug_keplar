package week3.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("Testuser");
		driver.findElementById("userRegistrationForm:password").sendKeys("Testuser@123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Testuser@123");
		WebElement src=  driver.findElementById("userRegistrationForm:securityQ");
		Select drop1=new Select(src);
		drop1.selectByVisibleText("Who was your Childhood hero?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("MyDad");
		WebElement lang=  driver.findElementById("userRegistrationForm:prelan");
		Select drop2=new Select(lang);
		drop2.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("N");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("K");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Dhanapal");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement day =  driver.findElementById("userRegistrationForm:dobDay");
		Select drop3=new Select(day);
		drop3.selectByVisibleText("29");
		WebElement month =  driver.findElementById("userRegistrationForm:dobMonth");
		Select drop4=new Select(month);
		drop4.selectByVisibleText("DEC");
		WebElement year =  driver.findElementById("userRegistrationForm:dateOfBirth");
		Select drop5=new Select(year);
		drop5.selectByVisibleText("1991");
		WebElement occ =  driver.findElementById("userRegistrationForm:occupation");
		Select drop6=new Select(occ);
		drop6.selectByVisibleText("Private");
		WebElement country =  driver.findElementById("userRegistrationForm:countries");
		Select drop7=new Select(country);
		drop7.selectByValue("94");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("1234 1234 1234");
		driver.findElementById("userRegistrationForm:idno").sendKeys("BZXPD7322K");
		driver.findElementById("userRegistrationForm:email").sendKeys("testuser@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("91",Keys.TAB);
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9677488871");
		WebElement nationality =  driver.findElementById("userRegistrationForm:nationalityId");
		Select drop8=new Select(nationality);
		drop8.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("Chennai");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600049",Keys.TAB);
		Thread.sleep(5000);
		WebElement city =  driver.findElementById("userRegistrationForm:cityName");
		Select drop9=new Select(city);
		drop9.selectByIndex(1);
		Thread.sleep(5000);
		WebElement postoffice =  driver.findElementById("userRegistrationForm:postofficeName");
		Select drop10=new Select(postoffice);
		drop10.selectByIndex(5);
		driver.findElementById("userRegistrationForm:landline").sendKeys("235994");
		driver.findElementById("userRegistrationForm:resAndOff:1").click();
		//Office address
		WebElement country2 =  driver.findElementById("userRegistrationForm:countrieso");
		Select drop11=new Select(country2);
		drop11.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:addresso").sendKeys("chennai");
		driver.findElementById("userRegistrationForm:pincodeo").sendKeys("600113",Keys.TAB);
		Thread.sleep(5000);
		WebElement city2 =  driver.findElementById("userRegistrationForm:cityNameo");
		Select drop12=new Select(city2);
		drop12.selectByIndex(1);
		
		Thread.sleep(5000);
		WebElement postoffice2 =  driver.findElementById("userRegistrationForm:postofficeNameo");
		Select drop13=new Select(postoffice2);
		drop13.selectByIndex(1);
		driver.findElementById("userRegistrationForm:landlineo").sendKeys("235994");
		
    //    driver.close();
		
	}

}
