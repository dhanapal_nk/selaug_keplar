package project;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;

public class TC001_Facebook
{
	
	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com");
		driver.findElementById("email").sendKeys("9677488871");
		driver.findElementByXPath("(//input[@type='password'])[1]").sendKeys("29121991_nk");
		driver.findElementByXPath("//input[@type='submit']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@data-testid='search_input']").sendKeys("Testleaf");
		Thread.sleep(3000);
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String searchResult = driver.findElementByXPath("(//div[text()='TestLeaf'])[1]").getText();
		if(searchResult.contains("TestLeaf"))
		{
			System.out.println("Search Result is : "+ searchResult);
		}
		driver.findElementByXPath("(//div[text()='TestLeaf'])[1]").click();
		String likeValue = driver.findElementByXPath("//button[contains(text(),page_profile_like)]").getText();
		if(likeValue.equals("Like"))
		{
			System.out.println("Button is : "+likeValue);
			driver.findElementByXPath("//button[contains(text(),page_profile_like)]").click();
		}
		else
		{
			System.out.println("Page is Liked already");
		}
		String title = driver.getTitle();
		if(title.contains("TestLeaf"))
		{
			System.out.println("The title has TestLeaf");
		}
		else
		{
			System.out.println("The title does not have TestLeaf");
		}
		Thread.sleep(7000);
		String noOfLikes = driver.findElementByXPath("//div[contains(text(),'people like this')]").getText().replaceAll("\\D", "");
		System.out.println("No of Likes: "+noOfLikes);

	}
}
