package project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.lang.model.element.Element;

import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC002_ZoomCar extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC002_ZoomCar";
		testCaseDesc="To Book a Zoom Car and get maximum price Car name";
		category="smoke";
		author="Dhanapal";
	}

	@Test
	public void Zoomcar() throws InterruptedException {
		
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement eleStartLink = locateElement("linkText", "Start your wonderful journey");
		click(eleStartLink);
		Thread.sleep(2000);
		WebElement eleItem = locateElement("xpath", "(//div[@class='items'])[1]");
		click(eleItem);
		WebElement eleProceed = locateElement("class", "proceed");
		click(eleProceed);
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today = sdf.format(date);
		int tomorrow=Integer.parseInt(today)+1;
		WebElement eleDate = locateElement("xpath", "//div[contains(text(),"+tomorrow+")]/div");
		click(eleDate);
		WebElement eleSubmit = locateElement("class", "proceed");
		click(eleSubmit);
		WebElement eleDone = locateElement("xpath", "//button[text()='Done']");
		click(eleDone);
		Thread.sleep(3000);
		locateElement("xpath", "//div[@class='price']");
		List<WebElement> carList = locateElements("xpath", "//div[@class='price']");
		List<Integer> lst=new ArrayList<>();
		for (WebElement webElement : carList) {
			String price = webElement.getText().replaceAll("\\D", "");
			System.out.println(price);
			lst.add(Integer.parseInt(price));
		}
		int count = lst.size();
		System.out.println("Total records:"+ count);
		Integer max = Collections.max(lst);
		System.out.println(max);
		String text = locateElement("xpath", "//div[contains(text(),"+max+")]/preceding::h3[1]").getText();
		System.out.println(text);
		WebElement eleEnd = locateElement("xpath", "//div[contains(text(),"+max+")]/following::button");
		click(eleEnd);
	}
}
