package project;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class FB extends ProjectMethods
{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC001_Facebook";
		testCaseDesc="To Like TestLeaf page and get the like count";
		category="smoke";
		author="Dhanapal";
	}
	
	@Test
	public void fbLogin() throws InterruptedException {
		
		/*startApp("chrome", "https://www.facebook.com/");
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com");*/
		startApp("chrome", "https://www.facebook.com/");
		WebElement eleEmail = locateElement("Id", "email");
		type(eleEmail,"9677488871");
		//driver.findElementById("email").sendKeys("9677488871");
		WebElement elePassword = locateElement("xpath", "//input[@type='password'])[1]");
		type(elePassword,"29121991_nk");
		//driver.findElementByXPath("(//input[@type='password'])[1]").sendKeys("29121991_nk");
		WebElement eleSubmit = locateElement("xpath","//input[@type='submit']");
		click(eleSubmit);
		//driver.findElementByXPath("//input[@type='submit']").click();
		Thread.sleep(3000);
		WebElement eleText = locateElement("xpath", "//input[@data-testid='search_input']");
		type(eleText, "Testleaf");
		//driver.findElementByXPath("//input[@data-testid='search_input']").sendKeys("Testleaf");
		Thread.sleep(3000);
		WebElement eleSearchButton = locateElement("xpath", "//button[@data-testid='facebar_search_button']");
		click(eleSearchButton);
		//driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String searchResult = locateElement("xpath", "(//div[text()='TestLeaf'])[1]").getText();
		//String searchResult = driver.findElementByXPath("(//div[text()='TestLeaf'])[1]").getText();
		if(searchResult.contains("TestLeaf"))
		{
			System.out.println("Search Result is : "+ searchResult);
		}
		locateElement("xpath","(//div[text()='TestLeaf'])[1]").click();
		//driver.findElementByXPath("(//div[text()='TestLeaf'])[1]").click();
		String likeValue = locateElement("xpath", "//button[contains(text(),page_profile_like)]").getText();
		//String likeValue = driver.findElementByXPath("//button[contains(text(),page_profile_like)]").getText();
		if(likeValue.equals("Like"))
		{
			System.out.println("Button is : "+likeValue);
			locateElement("xpath", "//button[contains(text(),page_profile_like)]").click();
			//driver.findElementByXPath("//button[contains(text(),page_profile_like)]").click();
		}
		else
		{
			System.out.println("Page is Liked already");
		}
		String title = driver.getTitle();
		if(title.contains("TestLeaf"))
		{
			System.out.println("The title has TestLeaf");
		}
		else
		{
			System.out.println("The title does not have TestLeaf");
		}
		Thread.sleep(7000);
		String noOfLikes = locateElement("xpath", "//div[contains(text(),'people like this')]").getText().replaceAll("\\D", "");
		//String noOfLikes = driver.findElementByXPath("//div[contains(text(),'people like this')]").getText().replaceAll("\\D", "");
		System.out.println("No of Likes: "+noOfLikes);

	}
}
