package week2.day2;

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class ClassWorkSet {

	public static void main(String[] args) {
		Set <String> mobiles=new LinkedHashSet<String>();
		mobiles.add("Nokia");
		mobiles.add("Moto");
		mobiles.add("Nokia");
		mobiles.add("Oppo");
		System.out.println("List of Mobiles in Set");
		for (String eachMobiles : mobiles) {
			System.out.println(eachMobiles);
		}
		List<String> mobileList=new ArrayList<String>();
		mobileList.addAll(mobiles);
		System.out.println("List of Mobiles in List");
		for (String string : mobileList) {
			System.out.println(string);
		}
		String firstMobile=mobileList.get(0);
		System.out.println("FirstMobile: "+firstMobile);

	}

}
