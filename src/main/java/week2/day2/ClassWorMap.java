package week2.day2;

import java.util.HashMap;
import java.util.Map;

public class ClassWorMap {

	public static void main(String[] args) {
		String name="Dhanapal";
		char[] charArray = name.toCharArray();
		System.out.println(charArray);
		Map<Character,Integer> nameMap=new HashMap<Character,Integer>();
		for (char c : charArray) {
			if(nameMap.containsKey(c))
			{
				System.out.println(c +" is duplicated");
			}
			else 
			{
			nameMap.put(c, 1);
			}
		}
	}

}
