package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListArrayConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name="Amazon India";
		List<Character> ascList=new ArrayList<Character>();
		int len=name.length();
		for(int i=0;i<len;i++)
		{
			ascList.add(name.charAt(i));
		}
		Collections.sort(ascList);
		System.out.println("After ascending order");
		for (Character character : ascList) {
			System.out.println(character);	
		}
		List<Character> desList=new ArrayList<Character>();
		name=name.toLowerCase();
		int length=name.length();
		for(int i=0;i<length;i++)
		{
			desList.add(name.charAt(i));
		}
		Collections.reverse(desList);
		System.out.println("After Descending order");
		for (Character character : desList) {
			System.out.println(character);	
		}
		List<Character> dupList=new ArrayList<Character>();
		char[] charArray = name.toLowerCase().toCharArray();
		for (Character eachChar : charArray) {
			if(!dupList.contains(eachChar))
			{
				dupList.add(eachChar);
			}
		}
		System.out.println("Removed Duplicates in string:");
		Collections.sort(dupList);
		System.out.println(dupList);	
	}
}