package week2.day1;

public class MobilePhone extends TeleCom implements MobileDesign{

	public void sendSMS() 
	{
		System.out.println("Message Sent");
	}
	public void camera()
	{
		System.out.println("true");
	}
	@Override
	public void Support4G() {
		System.out.println("My Phone supporting 4G");
		
	}
	@Override
	public void EnableWiFi() {
		System.out.println("WiFi is enabled");
		
	}
	@Override
	public void SupportFingerprint() {
		System.out.println("My Phone is supporting Fingerprint");
		
	}

}
