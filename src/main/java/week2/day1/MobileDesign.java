package week2.day1;

public interface MobileDesign {
	
	public void Support4G();
	public void EnableWiFi();
	public void SupportFingerprint();
}
