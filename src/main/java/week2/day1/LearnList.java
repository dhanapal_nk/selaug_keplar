package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> myPhones=new ArrayList<String>();
		myPhones.add("Nokia 3100");
		myPhones.add("Nokia 6233");
		myPhones.add("moto G2");
		myPhones.add("Oppo F3");
		System.out.println("My Mobiles List:"); 
		for (String eachPhone : myPhones) {
			System.out.println(eachPhone);
		}
		int count=myPhones.size();
		System.out.println("Total Mobile count: "+ count);
		System.out.println("Last but one Mobile Phone: "+myPhones.get(count-2));
		Collections.sort(myPhones);
		System.out.println("My Mobiles in sorted order:") ;
		for (String sortPhones : myPhones) {
			System.out.println(sortPhones);
		}
		
	}

}
