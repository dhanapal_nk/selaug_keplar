Feature: LogIn for LeafTaps

#Background:
#Given Open the Chrome Browser
#And Maximize the browser
#And Set Timeouts
#And Hit the URL

Scenario Outline: Positive CreateLead flow

And Enter the Username as <userName>
And Enter the Password as <password>
And Click on the login button
And Click on crmsfa
And Click on Leads
And Click on CreateLead
And Enter the Company Name as <companyName>
And Enter the First Name as <firstName>
And Enter the Last Name as <lastName>
When Click on Creat Lead Button
Then New Lead is created with first name as <vfName>

Examples:
|userName|password|companyName|firstName|lastName|vfName|
|DemoSalesManager|crmsfa|ABC|Dhanapal|K|Dhanapal|
|DemoCSR|crmsfa|XYZ|Sai|Charan|Sai|
