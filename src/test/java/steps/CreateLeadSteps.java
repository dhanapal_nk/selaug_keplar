/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {
	
	public static ChromeDriver driver;
	@Given("Open the Chrome Browser")
	public void openBrowser(){
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
	}
	
	@And("Maximize the browser")
	public void maxBrowser() {
		driver.manage().window().maximize();
	}
	
	@And("Set Timeouts")
	public void setTimeouts(){
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	@And("Hit the URL")
	public void loadURL(){
		driver.get("http://leaftaps.com/opentaps");
	}
	
	@And("Enter the Username as (.*)")
	public void enterUsername(String username) {
		driver.findElementById("username").sendKeys(username);
	}
	
	@And("Enter the Password as (.*)")
	public void enterPassword(String password) {
		driver.findElementById("password").sendKeys(password);
	}
	
	@When("Click on the login button")
	public void clickLogin() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Click on crmsfa")
	public void clickCRMSFA(){
		driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@And("Click on Leads")
	public void clickLeads(){
		driver.findElementByLinkText("Leads").click();
	}
	
	@And("Click on CreateLead")
	public void clickCreateLead(){
		driver.findElementByLinkText("Create Lead").click();
	}
	@And("Enter the Company Name as (.*)")
	public void enterCompanyName(String companyName){
		driver.findElementById("createLeadForm_companyName").sendKeys(companyName);	
	}
	
	@And("Enter the First Name as (.*)")
	public void enterFirstName(String firstName){
		driver.findElementById("createLeadForm_firstName").sendKeys(firstName);	
	}
	
	@And("Enter the Last Name as (.*)")
	public void enterLastName(String lastName){
		driver.findElementById("createLeadForm_lastName").sendKeys(lastName);	
	}
	
	@When("Click on Creat Lead Button")
	public void clickCL(){
		driver.findElementByClassName("smallSubmit").click();
	}
	
	@Then("New Lead is created")
	public void newLead() {
		System.out.println("New lead is created successfully");
	}
}
*/