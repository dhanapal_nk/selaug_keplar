package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdmethods.SeMethods;

public class Hooks extends SeMethods {

	@Before
	public void beforeScenario(Scenario sc) {
		System.out.println(sc.getName());
		System.out.println(sc.getId());
		beginResult();
		testCaseName= sc.getName();
		testCaseDesc= sc.getId();
		category= "smoke";
		author= "Dhanapal";
		startTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	@After
	public void afterScenario(Scenario sc) {
		System.out.println(sc.getStatus());
		System.out.println(sc.isFailed());
		closeAllBrowsers();
		endResult();
	}
	
}
