package testNG;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdmethods.ProjectMethods;

public class TC003_MergeLeads extends ProjectMethods{

	@BeforeTest(groups= {"regression"})
	public void setData() {
		testCaseName="TC003_MergeLeads";
		testCaseDesc="Merging two Lead";
		category="smoke";
		author="Dhanapal";
	}

	@Test(groups= {"regression"},dataProvider="positive")
	public void MLead(String fName, String sName) throws InterruptedException
	{
		WebElement eleLeads = locateElement("xpath","//a[text()='Leads']");
		click(eleLeads);
		WebElement eleMLeads = locateElement("linkText","Merge Leads");
		click(eleMLeads);
		WebElement eleFLead = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[1]");
		click(eleFLead);
		switchToWindow(1);
		WebElement eleFLeadName = locateElement("xpath", "//input[@name='firstName']");
		type(eleFLeadName, fName);
		WebElement eleFLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFLeads);
		Thread.sleep(3000);
		WebElement eleFRecord = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(eleFRecord);
		switchToWindow(0);
		WebElement eleSLead = locateElement("xpath","(//img[@src='/images/fieldlookup.gif'])[2]");
		click(eleSLead);
		switchToWindow(1);
		WebElement eleSLeadName = locateElement("xpath", "//input[@name='firstName']");
		type(eleSLeadName, sName);
		WebElement eleSLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleSLeads);
		Thread.sleep(3000);
		WebElement eleSRecord = locateElement("xpath", "(//a[@class='linktext'])[1]");
		clickWithNoSnap(eleSRecord);
		switchToWindow(0);
		WebElement eleMerge = locateElement("linkText", "Merge");
		click(eleMerge);
		acceptAlert();
	}

	@DataProvider(name="positive")
	public Object[][] fetchData(){
		Object[][] data = new Object[1][2];

		data[0][0]="dhanapal";
		data[0][1]="mahesh";

		return data;
	}

	@DataProvider(name="negative")
	public void fetchNegData(){

	}

}
